﻿Feature: Loading A Page

Scenario: Loading A Page
	When I request the page 'http://localhost:54068/'
	Then the current page should be 'http://localhost:54068/'

Scenario: Page Contains an Image
	When I request the page 'http://localhost:54068/'
	Then the page should contain an 'image'
	
Scenario: Page Contains Multiple Images
	When I request the page 'http://localhost:54068/'
	Then the page should contain '2' 'images'

Scenario: Page Contains a Link
	When I request the page 'http://localhost:54068/'
	Then the page should contain a 'link'

Scenario: Page Contains Multiple Links
	When I request the page 'http://localhost:54068/'
	Then the page should contain '2' 'links'

Scenario: Page Contains a List
	When I request the page 'http://localhost:54068/'
	Then the page should contain a 'list'

Scenario: Page Contains an Ordered List
	When I request the page 'http://localhost:54068/'
	Then the page should contain an 'ordered list'

Scenario: Page Contains an Unordered List
	When I request the page 'http://localhost:54068/'
	Then the page should contain an 'unordered list'

Scenario: Page Contains a Heading
	When I request the page 'http://localhost:54068/'
	Then the page should contain an 'heading'

Scenario: Page Contains a Heading with text
	When I request the page 'http://localhost:54068/'
	Then the page should contain an 'heading' with the text 'Some Heading'