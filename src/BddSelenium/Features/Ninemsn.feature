﻿Feature: Ninemsn Portal Home

Background: 
	When I request the page 'http://ninemsn.com.au/'

Scenario: The page should contain
	Then the page should contain a 'shortcuts' 'panel'
	
	And the page should contain a 'header' 'panel'
	And the 'header' 'panel' should contain a 'logo' 'panel'
	And the 'logo' 'panel' should contain '1' 'images'
	And the page should contain a 'srchfrm' 'form'
	
	And the page should contain a 'nav' 'panel'
	And the 'nav' 'panel' should contain '2' 'lists'
	
	And the 'left' 'panel' should contain a 'rail' 'panel'
	And the 'left' 'panel' should contain a 'news' 'panel'
	And the 'left' 'panel' should contain a 'tabloidNews_module' 'panel'
	And the 'left' 'panel' should contain a 'vote' 'panel'
	And the 'left' 'panel' should contain a 'ad_strapad' 'panel'
	And the 'left' 'panel' should contain a 'finance' 'panel'
	And the 'left' 'panel' should contain a 'sport' 'panel'

	And the page should contain a 'pick' 'panel'

	And the page should contain a 'feed' 'panel'

	And the page should contain a 'right' 'panel'
	And the 'right' 'panel' should contain a 'weather' 'panel'
	And the 'right' 'panel' should contain a 'partners' 'list'
	And the 'right' 'panel' should contain an 'ad_medium' 'panel'
	#match sponsor
	And the 'right' 'panel' should contain a 'tv' 'panel'
	And the 'right' 'panel' should contain a 'most-searched' 'panel'
	And the 'right' 'panel' should contain a 'our-magazines' 'panel'
	And the 'right' 'panel' should contain a 'marketing_ad_RHC' 'panel'
	And the 'right' 'panel' should contain a 'fb-like' 'panel'
	And the 'right' 'panel' should contain a 'shopping' 'panel'
	# yahoo sponsored links
	And the 'right' 'panel' should contain a 'ad_halfpage' 'panel'

	And the page should contain a 'nh_footer' 'panel'
