﻿namespace BddSelenium.StepDefinitions
{
    using System.Collections.Generic;
    using System.Linq;

    using FluentAssertions;

    using TechTalk.SpecFlow;

    [Binding]
    public class PageSteps
    {
        [When(@"I request the page '([^']*)'")]
        public void WhenIRequestThePagX(string url)
        {
            Browser.Current.Navigate().GoToUrl(url);
        }

        [Then(@"the current page should be '([^']*)'")]
        public void ThenTheCurrentPageShouldBe(string url)
        {
            Browser.Current.Url.Should().Be(url);
        }

        [Then(@"the page should contain an? '([^']*)'")]
        public void ThenThePageShouldContainAnX(string elementType)
        {
            GetFirstCell(elementType);
        }

        [Then(@"the page should contain an? '([^']*)' '([^']*)'")]
        public void ThenThePageShouldContainAnXWithClassY(string elementClass, string elementType)
        {
            GetFirstCell(elementType, elementClass);
        }

        [Then(@"the page should contain '([^']*)' '([^']*)s'")]
        public void ThenThePageShouldContainXYs(int expectedCount, string elementType)
        {
            GetCells(elementType).Should().HaveCount(expectedCount);
        }

        [Then(@"the page should contain an? '([^']*)' with the text '([^']*)'")]
        public void ThenThePageShouldContainAnXWithTheTextY(string elementType, string text)
        {
            GetFirstCell(elementType).Text.Should().Be(text);
        }

        [Then(@"the page should contain an? '([^']*)' with:")]
        public void ThenThePageShouldContainAnXWith(string elementType, Table table)
        {
            var firstCell = GetFirstCell(elementType);
            foreach (var row in table.Rows)
            {
                foreach (var column in table.Header)
                {
                    var value = firstCell.GetValue(column);
                    value.Should().NotBeNull("{0} does not contain a value for {1}", elementType, column);
                    value.Should().Be(row[column]); 
                }
            }
        }
        
        [Then(@"the '([^']*)' '([^']*)' should contain '([^']*)' '([^']*)s'")]
        public void ThenParentCellShouldContainXChildCells(string parentElementClass, string parentElementType, int childElementExpectedCount, string childElementType)
        {
            var parent = GetFirstCell(parentElementType, parentElementClass);
            CellFactory.GetCells(parent, childElementType).Should().HaveCount(childElementExpectedCount);
        }
        
        [Then(@"the '([^']*)' '([^']*)' should contain an? '([^']*)' '([^']*)'")]
        public void ThenTheParentCellShouldContainAChildCell(string parentElementClass, string parentElementType, string childElementClass, string childElementType)
        {
            var parent = GetFirstCell(parentElementType, parentElementClass);
            CellFactory.GetCells(parent, childElementType, childElementClass).Should().NotBeEmpty();
        }

        private static IEnumerable<ICell> GetCells(string elementType)
        {
            return CellFactory.GetCells(elementType);
        }

        private static IEnumerable<ICell> GetCells(string elementType, string elementClass)
        {
            return CellFactory.GetCells(elementType, elementClass);
        }

        private static ICell GetFirstCell(string elementType)
        {
            var cells = GetCells(elementType).ToArray();
            cells.Should().NotBeEmpty("Could not find a '{0}'", elementType);
            return cells.First();
        }

        private static ICell GetFirstCell(string elementType, string elementClass)
        {
            var cells = GetCells(elementType, elementClass).ToArray();
            cells.Should().NotBeEmpty("Could not find a '{0}' with the class '{1}'", elementType, elementClass);
            return cells.First();

        }
    }
}