namespace BddSelenium.StepDefinitions.Cells
{
    using OpenQA.Selenium;

    public class PageCell : ElementCell
    {
        public PageCell() : base(Browser.Current.FindElement(By.TagName("html")))
        {
            
        }
    }
}