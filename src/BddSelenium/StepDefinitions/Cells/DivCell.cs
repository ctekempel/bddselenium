namespace BddSelenium.StepDefinitions.Cells
{
    using System.Collections.Generic;

    using OpenQA.Selenium;

    public class DivCell : ElementCell
    {
        public DivCell(IWebElement element)
            : base(element)
        {
        }

        public static IEnumerable<ICell> GetCells(ICell parentCell, string elementClassOrId)
        {
            return ElementCell.GetCells(parentCell, "div", elementClassOrId);
        }
    }
}