namespace BddSelenium.StepDefinitions.Cells
{
    using System.Collections.Generic;

    using OpenQA.Selenium;

    public class FormCell : ElementCell
    {
        public FormCell(IWebElement element)
            : base(element)
        {
        }

        public static IEnumerable<ICell> GetCells(ICell parentCell, string elementClassOrId)
        {
            return ElementCell.GetCells(parentCell, "form", elementClassOrId);
        }
    }
}