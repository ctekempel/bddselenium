namespace BddSelenium.StepDefinitions
{
    using System;
    using System.Collections.Generic;

    using BddSelenium.StepDefinitions.Cells;

    public class CellFactory
    {
        public static Dictionary<string, Func<ICell, string, IEnumerable<ICell>>> CellMappings =
            new Dictionary<string, Func<ICell, string, IEnumerable<ICell>>>
                {
                    { "image", ImageCell.GetCells },
                    { "link", LinkCell.GetCells },
                    { "list", ListCell.GetCells },
                    { "unordered list", UnorderedListCell.GetCells },
                    { "ordered list", OrderedListCell.GetCells },
                    { "heading", HeadingCell.GetCells },
                    { "panel", DivCell.GetCells },
                    { "form", FormCell.GetCells },
                    { "navigation", NavigationCell.GetCells },
                    { "footer", FooterCell.GetCells }
                };

        public static IEnumerable<ICell> GetCells(string elementType)
        {
            return GetCells(null, elementType, null);
        }
        
        public static IEnumerable<ICell> GetCells(ICell parentCell, string elementType)
        {
            return GetCells(parentCell, elementType, null);
        }

        public static IEnumerable<ICell> GetCells(string elementType, string elementClassOrId)
        {
            return GetCells(null, elementType, elementClassOrId);
        }

        public static IEnumerable<ICell> GetCells(ICell parentCell, string elementType, string elementClassOrId)
        {
            var cellMapping = CellMappings[elementType];

            if (parentCell == null)
            {
                parentCell = new PageCell();
            }

            if (cellMapping != null)
            {
                return cellMapping(parentCell, elementClassOrId);
            }

            throw new InvalidOperationException(string.Format("Cell type {0} cannot be created.", elementType));
        }
    }
}