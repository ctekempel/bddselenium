namespace BddSelenium.StepDefinitions.Cells
{
    using System.Collections.Generic;

    using OpenQA.Selenium;

    public class HeadingCell : ElementCell
    {
        public HeadingCell(IWebElement element)
            : base(element)
        {
        }

        public static IEnumerable<ICell> GetCells(ICell parentCell, string elementClassOrId)
        {
            var tags = new[] { "h1", "h2", "h3", "h4", "h5", "h6" };

            return GetCells(parentCell, elementClassOrId, tags);

        }
    }
}