namespace BddSelenium.StepDefinitions
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Firefox;

    public class Browser
    {
        private static IWebDriver currentBrowser;

        public static IWebDriver Current
        {
            get
            {
                return currentBrowser ?? (currentBrowser = new FirefoxDriver());
            }
        }

    }
}