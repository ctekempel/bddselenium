namespace BddSelenium.StepDefinitions
{
    using TechTalk.SpecFlow;

    [Binding]
    public class TestRun
    {
        [AfterTestRun]
        public static void AfterTestRun()
        {
            Browser.Current.Close();
        }
    }
}