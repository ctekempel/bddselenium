﻿Feature: Defining a basic page

Background: 
	When I request the page 'http://localhost:54068/basicPage.htm'

Scenario: The page should contain
	Then the page should contain a 'heading' with the text 'Some Heading'
	And the page should contain an 'image' with:
	| Src                                    | Alt       |
	| http://localhost:54068/images/logo.png | something |
	And the page should contain a 'navigation' 'list'
	And the 'navigation' 'list' should contain '4' 'links'

