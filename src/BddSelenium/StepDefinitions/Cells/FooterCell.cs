namespace BddSelenium.StepDefinitions.Cells
{
    using System.Collections.Generic;

    using OpenQA.Selenium;

    public class FooterCell : ElementCell
    {
        public FooterCell(IWebElement element)
            : base(element)
        {
        }

        public static IEnumerable<ICell> GetCells(ICell parentCell, string elementClassOrId)
        {
            return ElementCell.GetCells(parentCell, "footer", elementClassOrId);
        }
    }
}