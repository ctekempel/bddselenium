namespace BddSelenium.StepDefinitions.Cells
{
    using System.Collections.Generic;
    using System.Linq;

    using OpenQA.Selenium;

    public abstract class ElementCell: ICell
    {
        private readonly IWebElement element;

        protected ElementCell(IWebElement element)
        {
            this.element = element;
        }

        public string Text
        {
            get
            {
                return this.element.Text;
            }
        }

        public IWebElement Element
        {
            get
            {
                return this.element;
            }
        }

        public virtual string GetValue(string key)
        {
            return this.element.GetAttribute(key.ToLower());
        }

        protected static IEnumerable<ICell> GetCells(ICell parentCell, string tagName, string elementClassOrId)
        {
            IEnumerable<IWebElement> findElements;

            if (elementClassOrId != null)
            {
                findElements = parentCell.Element.FindElements(By.Id(elementClassOrId)).Where(webElement => webElement.TagName == tagName).ToList();

                if (!findElements.Any())
                {
                    findElements = parentCell.Element.FindElements(By.ClassName(elementClassOrId)).Where(webElement => webElement.TagName == tagName).ToList();
                }              
            }
            else
            {
                findElements = parentCell.Element.FindElements(By.TagName(tagName));               
            }

            return findElements.Select(element => new OrderedListCell(element));
        }

        protected static IEnumerable<ICell> GetCells(ICell parentCell, string elementClassOrId, string[] tags)
        {
            IEnumerable<IWebElement> findElements;

            if (elementClassOrId != null)
            {
                findElements = parentCell.Element.FindElements(By.Id(elementClassOrId)).Where(webElement => tags.Contains(webElement.TagName)).ToList();

                if (!findElements.Any())
                {
                    findElements = parentCell.Element.FindElements(By.ClassName(elementClassOrId)).Where(webElement => tags.Contains(webElement.TagName)).ToList();
                }
            }
            else
            {
                findElements = tags.SelectMany(tag => parentCell.Element.FindElements(By.TagName(tag))).ToList();
            }

            return findElements.Select(element => new ListCell(element));
        }
    }
}