namespace BddSelenium.StepDefinitions
{
    using OpenQA.Selenium;

    public interface ICell
    {
        string Text { get; }

        IWebElement Element { get; }

        string GetValue(string key);
    }
}