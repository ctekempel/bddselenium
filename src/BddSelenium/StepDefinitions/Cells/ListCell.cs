namespace BddSelenium.StepDefinitions.Cells
{
    using System.Collections.Generic;

    using OpenQA.Selenium;

    public class ListCell : ElementCell
    {
        public ListCell(IWebElement element)
            : base(element)
        {
        }

        public static IEnumerable<ICell> GetCells(ICell parentCell, string elementClassOrId)
        {
            var tags = new[] { "ul", "ol" };

            return GetCells(parentCell, elementClassOrId, tags);
        }
    }
}